using System.Numerics;

namespace GlobalGameJam
{
    public static class TransformExtensions
    {
        public static Vector2 ToVector2(this UnityEngine.Vector2 v)
        {
            return new Vector2(v.x, v.y);
        }
        
        public static Vector3 ToVector3(this UnityEngine.Vector3 v)
        {
            return new Vector3(v.x, v.y, v.z);
        }

        public static Vector2 ToVector2(this UnityEngine.Vector3 v)
        {
            return new Vector2(v.x, v.y);
        }

        public static UnityEngine.Vector2 ToUnityVector2(this Vector2 v)
        {
            return new UnityEngine.Vector2(v.X, v.Y);
        }

        public static UnityEngine.Vector2 ToUnityVector2(this Vector3 v)
        {
            return new UnityEngine.Vector2(v.X, v.Y);
        }

        public static UnityEngine.Vector3 ToUnityVector3(this Vector3 v)
        {
            return new UnityEngine.Vector3(v.X, v.Y, v.Z);
        }
    }
}