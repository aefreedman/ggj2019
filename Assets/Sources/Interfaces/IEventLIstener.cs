using Entitas;

namespace GlobalGameJam
{
    public interface IEventListener
    {
        void RegisterListeners(IEntity entity);
    }
}