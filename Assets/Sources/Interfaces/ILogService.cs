namespace GlobalGameJam
{
    public interface ILogService
    {
        void LogMessage(string message);
    }
}