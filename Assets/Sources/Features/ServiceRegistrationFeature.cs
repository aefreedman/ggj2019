using GlobalGameJam.Systems.Registrars;

namespace GlobalGameJam
{
    public sealed class ServiceRegistrationFeature : Feature
    {
        public ServiceRegistrationFeature(Contexts contexts, Services services)
        {
            Add(new RegisterLogServiceSystem(contexts, services.Log));
            Add(new RegisterViewServiceSystem(contexts, services.View));
        }
    }
}