using System.Collections.Generic;
using Entitas;

namespace GlobalGameJam.Sources.Systems
{
    public class LoadAssetSystem : ReactiveSystem<GameEntity>, IInitializeSystem
    {
        private readonly Contexts _contexts;
        private IViewService _viewService;

        public LoadAssetSystem(Contexts contexts) : base(contexts.game)
        {
            _contexts = contexts;
        }

        public void Initialize()
        {
            // grab the view service instance from the meta context
            _viewService = _contexts.meta.viewService.Instance;
        }

        protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context) => context.CreateCollector(GameMatcher.Asset);

        protected override bool Filter(GameEntity entity) => entity.hasAsset && !entity.hasView;


        protected override void Execute(List<GameEntity> entities)
        {
            foreach (var e in entities)
            {
                // call the view service to make a new view
                _viewService.LoadAsset(_contexts, e, e.asset.Value);
//                if (view != null) e.ReplaceView(view);
            }
        }
    }
}