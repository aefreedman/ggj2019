using System.Numerics;
using Entitas;

namespace GlobalGameJam.Systems
{
    public class TestMessageSystem : IInitializeSystem
    {
        private readonly GameContext _gameContext;

        public TestMessageSystem(Contexts contexts)
        {
            _gameContext = contexts.game;
        }

        public void Initialize()
        {
            var entity = _gameContext.CreateEntity();
            entity.AddDebugLog("Hello World");
            entity.AddAsset("Cube");
            entity.AddPosition(Vector3.One);
        }
    }
}