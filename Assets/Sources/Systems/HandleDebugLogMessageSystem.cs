using System.Collections.Generic;
using Entitas;
using GlobalGameJam;

public sealed class HandleDebugLogMessageSystem : ReactiveSystem<GameEntity>
{
    private readonly Contexts _contexts;
    private readonly ILogService _logService;

    public HandleDebugLogMessageSystem(Contexts context)
        : base(context.game)
    {
        _contexts = context;
//        _logService = context.meta.logService.Instance;
    }

    protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context) => context.CreateCollector(GameMatcher.DebugLog);

    protected override bool Filter(GameEntity entity) => !entity.isDestroy;

    protected override void Execute(List<GameEntity> entities)
    {
        foreach (var entity in entities)
        {
             _contexts.meta.logService.Instance.LogMessage(entity.debugLog.Message);
            entity.isDestroy = true;
        }
    }
}