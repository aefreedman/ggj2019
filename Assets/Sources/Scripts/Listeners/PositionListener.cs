using Entitas;
using UnityEngine;

namespace GlobalGameJam.Listeners
{
    public class PositionListener : MonoBehaviour, IEventListener, IPositionListener
    {
        private GameEntity _entity;

        public void RegisterListeners(IEntity entity)
        {
            _entity = (GameEntity) entity;
            _entity.AddPositionListener(this);
        }

        public void OnPosition(GameEntity entity, System.Numerics.Vector3 Value)
        {
            transform.position = Value.ToUnityVector2();
        }
    }
}