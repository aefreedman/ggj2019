using Entitas;
using UnityEngine;

namespace GlobalGameJam
{
    public class UnityGameView : MonoBehaviour, IViewController
    {
        private Contexts _contexts;
        private GameEntity _entity;

//        public Vector2 Position {
//            get {return transform.position.ToVector2D();} 
//            set {transform.position = value.ToVector2();}
//        }
//
//        public Vector2 Scale // as above but with tranform.localScale

        public System.Numerics.Vector3 Position
        {
            get { return transform.position.ToVector3(); }
            set { transform.position = value.ToUnityVector3(); }
        }

        public System.Numerics.Vector3 Scale { get; set; }

        public bool Active
        {
            get { return gameObject.activeSelf; }
            set { gameObject.SetActive(value); }
        }

        public void InitializeView(Contexts contexts, IEntity entity)
        {
            _contexts = contexts;
            _entity = (GameEntity) entity;
        }

        public void DestroyView()
        {
            Object.Destroy(this);
        }
    }
}