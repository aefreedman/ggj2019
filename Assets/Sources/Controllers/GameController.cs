﻿namespace GlobalGameJam
{
    /// <inheritdoc />
    /// <summary>
    ///     Component in the scene that acts as the API for other components in the scene to access non-monobehaviors
    ///     e.g. Gives access to Spawners so button events can create windows
    /// </summary>
    public class GameController : AbstractGameController<GameController>
    {
        protected override void Awake()
        {
            GameControllerDefinition = new MyGameControllerDefinition();
        }
    }
}