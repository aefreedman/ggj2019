﻿using Entitas;
using UnityEngine;

//[SuppressMessage("ReSharper", "UnusedMember.Local")]
public abstract class AbstractGameController<T> : MonoBehaviour where T : class
{
    protected IGameControllerDefinition GameControllerDefinition;
    protected Systems _systems;

    /// <summary>
    /// Initialize the game controller definition here
    /// </summary>
    protected abstract void Awake();

    protected virtual void Start()
    {
        var contexts = Contexts.sharedInstance;
        
        if (_systems == null)
            _systems = GameControllerDefinition.CreateSystems(contexts);
        
        _systems.ActivateReactiveSystems();
        _systems.Initialize();
    }

    private void Update()
    {
        _systems.Execute();
        _systems.Cleanup();
    }

    private void OnDestroy()
    {
        _systems.TearDown();
    }
}