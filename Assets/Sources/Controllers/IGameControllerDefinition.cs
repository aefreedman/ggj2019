﻿using Entitas;

public interface IGameControllerDefinition
{
    Systems CreateSystems(Contexts c);
}