using GlobalGameJam.Sources.Systems;
using GlobalGameJam.Systems;

namespace GlobalGameJam
{
    public class MyGameControllerDefinition : IGameControllerDefinition
    {
        public Entitas.Systems CreateSystems(Contexts c)
        {
            var services = new Services(new UnityDebugLogService(), new UnityViewService());
            var serviceFeature = new ServiceRegistrationFeature(c, services);
            var events = new GameEventSystems(c);

            // All systems need to be added to the all systems feature because of how AbstractGameController implements system
            // updating (not a limitation of Entitas, just an arbitrary implementation constraint)
            var ft = new Feature("All Systems");

            ft.Add(serviceFeature);
            ft.Add(new TestMessageSystem(c));
            ft.Add(new HandleDebugLogMessageSystem(c));
            ft.Add(new LoadAssetSystem(c));
            ft.Add(events);

            return ft;
        }
    }
}