namespace GlobalGameJam
{
    public class Services
    {
        public readonly ILogService Log;
        public readonly IViewService View;

        public Services(ILogService log, IViewService view)
        {
            Log = log;
            View = view;
        }
    }
}