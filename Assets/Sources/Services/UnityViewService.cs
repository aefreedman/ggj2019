using Entitas;
using UnityEngine;

namespace GlobalGameJam
{
    /// <summary>
    /// Instantiates Unity game objects that implement IViewController. If the Prefab doesn't have
    /// a viewcontroller, will simply instantiate the gameobject
    /// </summary>
    public class UnityViewService : IViewService
    {
        public void LoadAsset(Contexts contexts, IEntity entity, string assetName) {

            //Similar to before, but now we don't return anything. 
            var viewGo = Object.Instantiate(Resources.Load<GameObject>("Prefabs/" + assetName));
            if (viewGo == null) return;
            var viewController = viewGo.GetComponent<IViewController>();
            viewController?.InitializeView(contexts, entity);

            // except we add some lines to find and initialize any event listeners
            var eventListeners = viewGo.GetComponents<IEventListener>();
            foreach(var listener in eventListeners) {
                listener.RegisterListeners(entity);
            }
        }
    }
}