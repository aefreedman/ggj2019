using System.Numerics;
using Entitas;
using Entitas.CodeGeneration.Attributes;
using GlobalGameJam;
// ReSharper disable CheckNamespace

//namespace GlobalGameJam
//{

[Game]
public class DebugLog : IComponent
{
    public string Message;
}

[Game]
public class Destroy : IComponent { }

[Meta, Unique]
public sealed class LogServiceComponent : IComponent
{
    public ILogService Instance;
}

[Game]
public class AssetComponent : IComponent
{
    public string Value;
}

[Game]
public sealed class ViewComponent : IComponent
{
    public IViewController Instance;
}

[Game, Event(EventTarget.Self)]
public class PositionComponent : IComponent
{
    public Vector3 Value;
}

[Meta, Unique]
public sealed class ViewServiceComponent : IComponent
{
    public IViewService Instance;
}
//}