//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentLookupGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public static class GameComponentsLookup {

    public const int Asset = 0;
    public const int DebugLog = 1;
    public const int Destroy = 2;
    public const int Position = 3;
    public const int PositionListener = 4;
    public const int View = 5;

    public const int TotalComponents = 6;

    public static readonly string[] componentNames = {
        "Asset",
        "DebugLog",
        "Destroy",
        "Position",
        "PositionListener",
        "View"
    };

    public static readonly System.Type[] componentTypes = {
        typeof(AssetComponent),
        typeof(DebugLog),
        typeof(Destroy),
        typeof(PositionComponent),
        typeof(PositionListenerComponent),
        typeof(ViewComponent)
    };
}
